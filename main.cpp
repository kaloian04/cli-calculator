#include <iostream>
#include <math.h>

using namespace std;

int main() {
  
  float firstnum, secondnum, result;
  string op;

  cout << "Enter first number: "; cin >> firstnum;

  cout << "Enter operator (+; -; *; /; ^; sqrt): "; cin >> op;

  if (op == "sqrt") {
    result = sqrt(firstnum);
    cout << "Result: " << result << "\n";
    return 0;
  }
  
  cout << "Enter second number: "; cin >> secondnum;
  
  if (op == "+") { result = firstnum + secondnum; }
  if (op == "-") { result = firstnum - secondnum; }
  if (op == "*") { result = firstnum * secondnum; }
  if (op == "/") { result = firstnum / secondnum; }
  if (op == "^") { result = pow(firstnum, secondnum); }

  cout << "Result: " << result << "\n";
  return 0;
  
}
